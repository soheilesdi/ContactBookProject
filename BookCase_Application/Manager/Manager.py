from BookCase_Core.DTO.GetRequestDto import GetRequestDto
from BookCase_Core.DTO.CreateRequestDto import CreateRequestDto
from BookCase_Application.Common.Interfaces.IManager import IManager


class Manager(IManager):
    def __init__(self , IValidator , IDataRepository):
        self.IValidator = IValidator
        self.IDataRepository = IDataRepository

    def manageRequest(self , requestData):
        self.IValidator.setDataToValidate(requestData).AdminCheck().checkCreateMethod()

        if self.IValidator.adminUser:
            if self.IValidator.wantToCreate == False :
                return {"message" : "why dont you want to Create !?"}
            profileData = CreateRequestDto(
                requestData['username'] , 
                requestData['age'] , 
                requestData['phoneNumber'] ,
                requestData['books'])
            
            for book in profileData.books:
                self.IDataRepository.insert(profileData.username , profileData.phonenumber , profileData.age , book)
            return {"message" : "Successfully Created!"}
        
        else:
            information = GetRequestDto(self.IValidator.checkUserRequest())
            return self.IDataRepository.query(information)

