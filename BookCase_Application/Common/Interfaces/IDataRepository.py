import abc

class IDataRepository():
    def __init__(self , DataRepository):
        self.DataRepository = DataRepository

    
    @abc.abstractmethod
    def insert(self , username , phonenumber , age , book):
        raise NotImplementedError

    
    @abc.abstractmethod
    def query(self , information):
        raise NotImplementedError