import abc

#Interface of Manager
class IManager():
    def __init__(self , Manager):
        self.Manager = Manager


    @abc.abstractmethod
    def manageCreateRequest(self):
        raise NotImplementedError
