import abc

class IValidator():
    def __init__(self , Validator):
        self.Validator = Validator


    @abc.abstractmethod
    def setDataToValidate(self , requestData):
        raise NotImplementedError

    @abc.abstractmethod
    def AdminCheck(self):
        raise NotImplementedError


    @abc.abstractmethod
    def checkCreateMethod(self):
        raise NotImplementedError

    @abc.abstractmethod
    def checkUserRequest(self):
        raise NotImplementedError