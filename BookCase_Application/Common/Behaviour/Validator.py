from BookCase_Application.Common.Interfaces.IValidator import IValidator


class Validator(IValidator):
    def __init__(self):
        self.dataToValidate = dict()
        self.wantToCreate = False
        self.adminUser = False

    def setDataToValidate(self,  requestData):
        self.dataToValidate = requestData
        return self

    def AdminCheck(self):
        if 'created' in self.dataToValidate :
            self.adminUser = True
        else :
            self.adminUser = False
        return self
    
    def checkCreateMethod(self):
        if self.adminUser :
            if self.dataToValidate['created'] == True :
                self.wantToCreate = True
            else :
                self.wantToCreate = False
            return self
        else :
            return self

    def checkUserRequest(self):
        if self.dataToValidate['username'] != None :
            return {"key" :  "username", "value" : self.dataToValidate['username']}
        elif self.dataToValidate['phoneNumber'] != None :
            return {"key" :  "phonenumber", "value" : self.dataToValidate['phoneNumber']}
        else :
            return {"key" :  "book", "value" : self.dataToValidate['books']}

