from django.http.response import HttpResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from BookCase_Core.settings import services
import json

_manager = services.IManager

@api_view(['POST'])
def getMethod(requests):
    if requests.method == 'POST' :
        return HttpResponse(json.dumps(_manager.manageRequest(requests.data)))
    else :
        return HttpResponse(json.dumps({"message" : "is not ok!"}))