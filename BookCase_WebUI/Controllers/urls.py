from django.urls import path

from django.views.decorators.csrf import csrf_exempt
from . import RequestHandler

urlpatterns = [
    path('', csrf_exempt(RequestHandler.getMethod) , name = "getMethod"),
]