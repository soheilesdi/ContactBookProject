from BookCase_Application.Common.Interfaces.IDataRepository import IDataRepository
import sqlite3

class sqliteRepository(IDataRepository):
    def __init__(self):
        self.database = r"E:\Projects\ContactsBook\sqlite3.db"
        self.conn = sqlite3.connect(self.database)

        sql_create_BookCase_table = """ CREATE TABLE IF NOT EXISTS BookCase (
                                        id integer PRIMARY KEY,
                                        username text NOT NULL,
                                        phonenumber text,
                                        age text,
                                        book text
                                    ); """
        self.c = self.conn.cursor()
        self.c.execute(sql_create_BookCase_table)
        self.c.close()


    def insert(self, username , phonenumber , age , book):
        insertData = (username , phonenumber , age , book)
        sql = ''' INSERT INTO BookCase(username,phonenumber,age,book) VALUES(?,?,?,?) '''
        self.conn = sqlite3.connect(self.database)
        self.c = self.conn.cursor()
        self.c.execute(sql, insertData)
        self.conn.commit()
        self.conn.close()
        print("Data Successfully Inserted")
        
        
    def query(self, GetRequestDto):
        result = dict()
        self.conn = sqlite3.connect(self.database)
        self.c = self.conn.cursor()
        self.c.execute("SELECT * FROM BookCase where " + GetRequestDto.information['key'] + " =? ", (GetRequestDto.information['value'],) )

        rows = self.c.fetchall()

        if GetRequestDto.information['key'] == 'username' or GetRequestDto.information['key'] == 'phonenumber':
            result['books'] = list()
            for row in rows :
                result['books'].append(row[-1])

        else :
            result['username'] = list()
            for row in rows :
                result['username'].append(row[1])
            
        return result