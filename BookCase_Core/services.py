from BookCase_Application.Common.Interfaces.IDataRepository import IDataRepository
from BookCase_Application.Common.Interfaces.IValidator import IValidator
from BookCase_Application.Common.Interfaces.IManager import IManager

from BookCase_Repository.sqliteRepository import sqliteRepository
from BookCase_Application.Manager.Manager import Manager
from BookCase_Application.Common.Behaviour.Validator import Validator 


class Services():
    def __init__(self):
        self.IValidator = IValidator(Validator()).Validator
        self.IDataRepository = IDataRepository(sqliteRepository()).DataRepository
        self.IManager = IManager(Manager(self.IValidator , self.IDataRepository)).Manager
        