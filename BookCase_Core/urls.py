from django.urls import include, path
from rest_framework import routers
from BookCase_WebUI.Controllers import RequestHandler
router = routers.DefaultRouter()

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path(r'api/', include('BookCase_WebUI.Controllers.urls'))
]